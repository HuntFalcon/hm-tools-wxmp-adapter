package top.hmtools.wxmp.menu.message;

import org.junit.Test;

import top.hmtools.wxmp.core.DefaultWxmpMessageHandle;

/**
 * 模拟测试接收微信公众号自定义菜单相关事件
 * @author HyboWork
 *
 */
public class WxmpMessageTest {

	@Test
	public void executeMessageTest() {
		// 实例化消息处理handle
		DefaultWxmpMessageHandle defaultWxmpMessageHandle = new DefaultWxmpMessageHandle();

		// 实例化实际处理指定消息的controller，并加入handle映射
		MenuMessageTestController menuMessageTestController = new MenuMessageTestController();
		defaultWxmpMessageHandle.addMessageMetaInfo(menuMessageTestController);
		
		//点击菜单事件
		String xml = "<xml><ToUserName><![CDATA[toUser]]></ToUserName><FromUserName><![CDATA[FromUser]]></FromUserName><CreateTime>123456789</CreateTime><MsgType><![CDATA[event]]></MsgType><Event><![CDATA[CLICK]]></Event><EventKey><![CDATA[EVENTKEY]]></EventKey></xml>";
		defaultWxmpMessageHandle.processXmlData(xml);
	
		//点击菜单跳转链接时的事件推送
		xml = "<xml><ToUserName><![CDATA[toUser]]></ToUserName><FromUserName><![CDATA[FromUser]]></FromUserName><CreateTime>123456789</CreateTime><MsgType><![CDATA[event]]></MsgType><Event><![CDATA[VIEW]]></Event><EventKey><![CDATA[www.qq.com]]></EventKey><MenuId>MENUID</MenuId></xml>";
		defaultWxmpMessageHandle.processXmlData(xml);
	
		//scancode_push：扫码推事件的事件推送
		xml = "<xml><ToUserName><![CDATA[gh_e136c6e50636]]></ToUserName><FromUserName><![CDATA[oMgHVjngRipVsoxg6TuX3vz6glDg]]></FromUserName><CreateTime>1408090502</CreateTime><MsgType><![CDATA[event]]></MsgType><Event><![CDATA[scancode_push]]></Event><EventKey><![CDATA[6]]></EventKey><ScanCodeInfo><ScanType><![CDATA[qrcode]]></ScanType><ScanResult><![CDATA[1]]></ScanResult></ScanCodeInfo></xml>";
		defaultWxmpMessageHandle.processXmlData(xml);
		
		//pic_sysphoto：弹出系统拍照发图的事件推送
		xml = "<xml><ToUserName><![CDATA[gh_e136c6e50636]]></ToUserName><FromUserName><![CDATA[oMgHVjngRipVsoxg6TuX3vz6glDg]]></FromUserName><CreateTime>1408090651</CreateTime><MsgType><![CDATA[event]]></MsgType><Event><![CDATA[pic_sysphoto]]></Event><EventKey><![CDATA[6]]></EventKey><SendPicsInfo><Count>1</Count><PicList><item><PicMd5Sum><![CDATA[1b5f7c23b5bf75682a53e7b6d163e185]]></PicMd5Sum></item></PicList></SendPicsInfo></xml>";
		defaultWxmpMessageHandle.processXmlData(xml);
		
		//pic_photo_or_album：弹出拍照或者相册发图的事件推送
		xml = "<xml><ToUserName><![CDATA[gh_e136c6e50636]]></ToUserName><FromUserName><![CDATA[oMgHVjngRipVsoxg6TuX3vz6glDg]]></FromUserName><CreateTime>1408090816</CreateTime><MsgType><![CDATA[event]]></MsgType><Event><![CDATA[pic_photo_or_album]]></Event><EventKey><![CDATA[6]]></EventKey><SendPicsInfo><Count>1</Count><PicList><item><PicMd5Sum><![CDATA[5a75aaca956d97be686719218f275c6b]]></PicMd5Sum></item></PicList></SendPicsInfo></xml>";
		defaultWxmpMessageHandle.processXmlData(xml);
		
		//pic_weixin：弹出微信相册发图器的事件推送
		xml = "<xml><ToUserName><![CDATA[gh_e136c6e50636]]></ToUserName><FromUserName><![CDATA[oMgHVjngRipVsoxg6TuX3vz6glDg]]></FromUserName><CreateTime>1408090816</CreateTime><MsgType><![CDATA[event]]></MsgType><Event><![CDATA[pic_weixin]]></Event><EventKey><![CDATA[6]]></EventKey><SendPicsInfo><Count>1</Count><PicList><item><PicMd5Sum><![CDATA[5a75aaca956d97be686719218f275c6b]]></PicMd5Sum></item></PicList></SendPicsInfo></xml>";
		defaultWxmpMessageHandle.processXmlData(xml);
		
		//location_select：弹出地理位置选择器的事件推送
		xml= "<xml><ToUserName><![CDATA[gh_e136c6e50636]]></ToUserName><FromUserName><![CDATA[oMgHVjngRipVsoxg6TuX3vz6glDg]]></FromUserName><CreateTime>1408091189</CreateTime><MsgType><![CDATA[event]]></MsgType><Event><![CDATA[location_select]]></Event><EventKey><![CDATA[6]]></EventKey><SendLocationInfo><Location_X><![CDATA[23]]></Location_X><Location_Y><![CDATA[113]]></Location_Y><Scale><![CDATA[15]]></Scale><Label><![CDATA[ 广州市海珠区客村艺苑路 106号]]></Label><Poiname><![CDATA[]]></Poiname></SendLocationInfo></xml>";
		defaultWxmpMessageHandle.processXmlData(xml);
		
		
		//点击菜单跳转小程序的事件推送
		xml = "<xml><ToUserName><![CDATA[toUser]]></ToUserName><FromUserName><![CDATA[FromUser]]></FromUserName><CreateTime>123456789</CreateTime><MsgType><![CDATA[event]]></MsgType><Event><![CDATA[view_miniprogram]]></Event><EventKey><![CDATA[pages/index/index]]></EventKey><MenuId>MENUID</MenuId></xml>";
		defaultWxmpMessageHandle.processXmlData(xml);
		
		
		
		
		
		
		
		
		
		
		
		
		
	}
}
