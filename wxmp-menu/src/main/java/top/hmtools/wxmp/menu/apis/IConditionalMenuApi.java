package top.hmtools.wxmp.menu.apis;

import top.hmtools.wxmp.core.annotation.WxmpApi;
import top.hmtools.wxmp.core.annotation.WxmpMapper;
import top.hmtools.wxmp.core.enums.HttpMethods;
import top.hmtools.wxmp.core.model.ErrcodeBean;
import top.hmtools.wxmp.menu.models.conditional.ConditionalBean;
import top.hmtools.wxmp.menu.models.conditional.ConditionalMenuBean;
import top.hmtools.wxmp.menu.models.conditional.TryMatchParamBean;
import top.hmtools.wxmp.menu.models.simple.MenuWapperBean;

/**
 * 个性化菜单接口
 */
@WxmpMapper
public interface IConditionalMenuApi {

	/**
	 * 创建个性化菜单
	 */
	@WxmpApi(httpMethods=HttpMethods.POST,uri = "/cgi-bin/menu/addconditional")
	public ConditionalBean addConditional(ConditionalMenuBean conditionalMenuBean);
	
	/**
	 * 删除个性化菜单
	 * @param conditionalBean
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.POST,uri = "/cgi-bin/menu/delconditional")
	public ErrcodeBean delConditional(ConditionalBean conditionalBean);
	
	/**
	 * 测试个性化菜单匹配结果
	 * @param tryMatchParamBean
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.POST,uri = "/cgi-bin/menu/trymatch")
	public MenuWapperBean tryMatch(TryMatchParamBean tryMatchParamBean);
}
