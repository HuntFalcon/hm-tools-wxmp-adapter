package top.hmtools.wxmp.menu.models.simple;

import java.util.List;

import top.hmtools.wxmp.menu.models.conditional.ConditionalMenuBean;

/**
 * 自定义菜单目录包装类
 * @author HyboWork
 *
 */
public class MenuWapperBean {

	/**
	 * 缺省的自定义菜单
	 */
	private MenuBean menu;
	
	/**
	 * 个性化自定义菜单
	 */
	private List<ConditionalMenuBean> conditionalmenu;

	public MenuBean getMenu() {
		return menu;
	}

	public void setMenu(MenuBean menu) {
		this.menu = menu;
	}
	
	


	public List<ConditionalMenuBean> getConditionalmenu() {
		return conditionalmenu;
	}

	public void setConditionalmenu(List<ConditionalMenuBean> conditionalmenu) {
		this.conditionalmenu = conditionalmenu;
	}

	@Override
	public String toString() {
		return "MenuWapperBean [menu=" + menu + ", conditionalmenu=" + conditionalmenu + "]";
	}

	
	
}
