package top.hmtools.wxmp.message.group.model.tagGroupSend;

/**
 * 视频，请注意，此处视频的media_id需通过POST请求到下述接口特别地得到：
 * @author HyboWork
 *
 */
public class VideoTagGroupSendParam extends BaseTagGroupSendParam {

	private MediaId mpvideo;


	public MediaId getMpvideo() {
		return mpvideo;
	}


	public void setMpvideo(MediaId mpvideo) {
		this.mpvideo = mpvideo;
	}


	@Override
	public String toString() {
		return "VideoTagGroupSendParam [mpvideo=" + mpvideo + ", filter=" + filter + ", msgtype=" + msgtype + "]";
	}


}
