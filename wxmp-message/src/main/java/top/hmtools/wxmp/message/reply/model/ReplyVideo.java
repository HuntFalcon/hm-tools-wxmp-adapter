package top.hmtools.wxmp.message.reply.model;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 回复视频消息 中 的视频信息
 * @author hybo
 *
 */
public class ReplyVideo {

	/**
	 * 通过素材管理中的接口上传多媒体文件，得到的id
	 */
	@XStreamAlias("MediaId")
	private String mediaId;
	
	/**
	 * 视频消息的标题
	 */
	@XStreamAlias("Title")
	private String title;
	
	/**
	 * 视频消息的描述
	 */
	@XStreamAlias("Description")
	private String description;

	public String getMediaId() {
		return mediaId;
	}

	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "ReplyVideo [MediaId=" + mediaId + ", Title=" + title + ", Description=" + description + "]";
	}
	
	
}
