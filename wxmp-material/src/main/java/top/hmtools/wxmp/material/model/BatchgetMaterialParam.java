package top.hmtools.wxmp.material.model;

import top.hmtools.wxmp.material.enums.MediaType;

public class BatchgetMaterialParam {

	private MediaType type;
	
	private int offset;
	
	private int count;
	
	public BatchgetMaterialParam(MediaType type,int offset,int count) {
		this.type=type;
		this.offset=offset;
		this.count=count;
	}

	public MediaType getType() {
		return type;
	}

	public void setType(MediaType type) {
		this.type = type;
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int offset) {
		this.offset = offset;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	@Override
	public String toString() {
		return "BatchgetMaterialParam [type=" + type + ", offset=" + offset + ", count=" + count + "]";
	}
	
	
}
